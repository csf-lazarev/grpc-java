package net.dingo.sv.sign;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import net.dingo.sv.sign.services.SignService;

import java.io.IOException;
import java.util.Optional;

public class SignServer {
    public static void main(String[] args) {
        try {
            /* *
             * Do not forget to install maven. The grpc stub classes are generated when you run the protoc compiler
             * and it finds a service declaration in your proto file.
             * Do not forget the client must use the same port in order to connect to this server.
             * */
            String verHost = Optional.ofNullable(System.getenv("VER_HOST")).orElse("localhost");
            int verPort = Optional.ofNullable(System.getenv("VER_PORT")).map(Integer::parseInt).orElse(8998);
            int sigPort = Optional.ofNullable(System.getenv("SIG_PORT")).map(Integer::parseInt).orElse(8999);

            Server server =
                    ServerBuilder.forPort(sigPort).addService(new SignService(verHost, verPort)).build();
            server.start();
            System.out.println("Server started at " + server.getPort());
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            System.out.println("Error: " + e);
        }


    }
}
