package net.dingo.sv.sign.services;


import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import net.dingo.sv.sign.SignServiceGrpc;
import net.dingo.sv.sign.Signing;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.digests.Blake3Digest;
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator;
import org.bouncycastle.crypto.params.Ed25519KeyGenerationParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class SignService extends SignServiceGrpc.SignServiceImplBase {

    private final Map<String, Ed25519PrivateKeyParameters> signCache;
    private final VerifyServiceConnector connector;

    public SignService(String verifyConnectorHost, int verifyConnectorPort) {
        signCache = new HashMap<>();
        connector = new VerifyServiceConnector(verifyConnectorHost, verifyConnectorPort);
    }

    @Override
    public void genKeyPair(Signing.GenKeyPairRequest request, StreamObserver<Signing.GenKeyPairResponse> responseObserver) {
        Ed25519KeyPairGenerator ed25519KeyPairGenerator = new Ed25519KeyPairGenerator();
        ed25519KeyPairGenerator.init(new Ed25519KeyGenerationParameters(null));
        AsymmetricCipherKeyPair asymmetricCipherKeyPair = ed25519KeyPairGenerator.generateKeyPair();

        Ed25519PrivateKeyParameters privateKeyParameters = (Ed25519PrivateKeyParameters) asymmetricCipherKeyPair.getPrivate();
        signCache.put(request.getCid(), privateKeyParameters);

        Ed25519PublicKeyParameters publicKeyParameters = (Ed25519PublicKeyParameters) asymmetricCipherKeyPair.getPublic();
        String encoded = Base64.getUrlEncoder().encodeToString(publicKeyParameters.getEncoded());
        connector.propagatePublicKey(request.getCid(), encoded);

        responseObserver.onNext(Signing.GenKeyPairResponse.newBuilder().setBase64PubKey(encoded).build());
        responseObserver.onCompleted();
    }

    @Override
    public void sign(Signing.SignRequest request, StreamObserver<Signing.SignResponse> responseObserver) {
        if (!signCache.containsKey(request.getCid())) {
            responseObserver.onError(Status.INVALID_ARGUMENT.asRuntimeException());
            return;
        }
        Ed25519PrivateKeyParameters ed25519PrivateKeyParameters = signCache.get(request.getCid());

        Ed25519Signer signer = new Ed25519Signer();
        signer.init(true, ed25519PrivateKeyParameters);
        byte[] digest = getDigest(request.getPayload());
        signer.update(digest, 0, digest.length);
        byte[] signature = signer.generateSignature();
        String encoded = Base64.getUrlEncoder().encodeToString(signature);
        responseObserver.onNext(Signing.SignResponse.newBuilder().setBase64Signature(encoded).build());
        responseObserver.onCompleted();
    }

    private byte[] getDigest(String payload) {
        byte[] bytes = payload.getBytes(StandardCharsets.UTF_8);
        Blake3Digest blake3Digest = new Blake3Digest();
        blake3Digest.update(bytes, 0, bytes.length);
        byte[] outDigest = new byte[blake3Digest.getDigestSize()];
        blake3Digest.doFinal(outDigest, 0);
        return outDigest;
    }
}
