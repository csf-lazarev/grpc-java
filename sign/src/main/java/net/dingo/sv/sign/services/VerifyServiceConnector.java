package net.dingo.sv.sign.services;

import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import net.dingo.sv.verify.Verification;
import net.dingo.sv.verify.VerifyServiceGrpc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VerifyServiceConnector {

    private final ManagedChannel signChannel;
    private final VerifyServiceGrpc.VerifyServiceBlockingStub stub;
    private final ExecutorService executor;

    public VerifyServiceConnector(String host, int port) {
        signChannel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
        stub = VerifyServiceGrpc.newBlockingStub(signChannel);
        executor = Executors.newFixedThreadPool(2);
    }

    public void propagatePublicKey(String cid, String encodedKey) {
        Empty empty = stub.pushPublicKey(Verification.PushPublicKeyRequest.newBuilder()
                .setCid(cid)
                .setBase64PubKey(encodedKey)
                .build());
        System.out.println("New key propagated");
    }

}
