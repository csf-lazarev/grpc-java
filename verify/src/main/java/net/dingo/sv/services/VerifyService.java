package net.dingo.sv.services;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import net.dingo.sv.verify.Verification;
import net.dingo.sv.verify.VerifyServiceGrpc;
import org.bouncycastle.crypto.digests.Blake3Digest;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class VerifyService extends VerifyServiceGrpc.VerifyServiceImplBase {

    private final Map<String, Ed25519PublicKeyParameters> verifyCache;

    public VerifyService() {
        verifyCache = new HashMap<>();
    }

    @Override
    public void verify(Verification.VerifyRequest request, StreamObserver<Verification.VerifyResponse> responseObserver) {
        if (!verifyCache.containsKey(request.getCid())) {
            responseObserver.onNext(Verification.VerifyResponse.newBuilder().setVerified(false).build());
            responseObserver.onCompleted();
            System.out.println("No found public key");
            return;
        }
        Ed25519Signer signer = new Ed25519Signer();
        signer.init(false, verifyCache.get(request.getCid()));

        byte[] digest = getDigest(request.getPayload());
        signer.update(digest, 0, digest.length);

        boolean verified = signer.verifySignature(Base64.getUrlDecoder().decode(request.getSignature()));
        responseObserver.onNext(Verification.VerifyResponse.newBuilder().setVerified(verified).build());
        responseObserver.onCompleted();
    }

    @Override
    public void pushPublicKey(Verification.PushPublicKeyRequest request, StreamObserver<Empty> responseObserver) {
        verifyCache.put(request.getCid(), fromBase64(request.getBase64PubKey()));
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }

    private Ed25519PublicKeyParameters fromBase64(String base64Key) {
        byte[] decode = Base64.getUrlDecoder().decode(base64Key);
        return new Ed25519PublicKeyParameters(decode);
    }

    private byte[] getDigest(String payload) {
        byte[] bytes = payload.getBytes(StandardCharsets.UTF_8);
        Blake3Digest blake3Digest = new Blake3Digest();
        blake3Digest.update(bytes, 0, bytes.length);
        byte[] outDigest = new byte[blake3Digest.getDigestSize()];
        blake3Digest.doFinal(outDigest, 0);
        return outDigest;
    }
}
