package net.dingo.sv.cid.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    private String cid;
    private String name;
    private String nameID;
    private String description;
}
