package net.dingo.sv.cid;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import net.dingo.sv.cid.service.CIDService;

import java.io.IOException;
import java.util.Optional;

public class CIDServer {
    public static void main(String[] args) {
        try {
            /* *
             * Do not forget to install maven. The grpc stub classes are generated when you run the protoc compiler
             * and it finds a service declaration in your proto file.
             * Do not forget the client must use the same port in order to connect to this server.
             * */
            int verPort = Optional.ofNullable(System.getenv("CID_PORT")).map(Integer::parseInt).orElse(8997);

            Server server =
                    ServerBuilder.forPort(verPort).addService(new CIDService()).build();
            server.start();
            System.out.println("Server started at " + server.getPort());
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }
}
