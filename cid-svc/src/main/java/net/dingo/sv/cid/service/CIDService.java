package net.dingo.sv.cid.service;

import com.google.rpc.Status;
import com.google.rpc.StatusProto;
import io.grpc.Metadata;
import io.grpc.stub.StreamObserver;
import net.dingo.sv.cid.CIDServiceGrpc;
import net.dingo.sv.cid.Ciding;
import net.dingo.sv.cid.domain.UserInfo;
import org.bouncycastle.crypto.SavableDigest;
import org.bouncycastle.crypto.digests.SHA256Digest;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;

public class CIDService extends CIDServiceGrpc.CIDServiceImplBase {

    private final Map<String, UserInfo> userInfoMap = new HashMap<>();

    @Override
    public void generateCID(Ciding.GenerateCID request, StreamObserver<Ciding.CIDResponse> responseObserver) {
        if (userInfoMap.containsKey(request.getNameID())
                && !userInfoMap.get(request.getNameID()).getNameID().equals(request.getCid())) {
            responseObserver.onError(io.grpc.Status.NOT_FOUND.asRuntimeException());
            return;
        }
        String cid = getCid();
        userInfoMap.put(request.getNameID(), new UserInfo(cid, request.getName(), request.getNameID(), request.getDescription()));
        responseObserver.onNext(Ciding.CIDResponse.newBuilder().setCid(cid).setNameID(request.getNameID()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getUserInfo(Ciding.GetUserInfo request, StreamObserver<Ciding.UserInfoResponse> responseObserver) {
        String nameID = request.getNameID();
        if (!userInfoMap.containsKey(nameID)) {
            responseObserver.onError(io.grpc.Status.NOT_FOUND.asRuntimeException());
            return;
        }
        UserInfo userInfo = userInfoMap.get(nameID);
        responseObserver.onNext(Ciding.UserInfoResponse.newBuilder()
                .setNameID(userInfo.getNameID())
                .setName(userInfo.getName())
                .setDescription(userInfo.getDescription())
                .setCid(userInfo.getCid())
                .build());
        responseObserver.onCompleted();
    }

    private static String getCid() {
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.concat(uuid);
        SavableDigest savableDigest = SHA256Digest.newInstance();
        byte[] bytes = uuid.getBytes();
        savableDigest.update(bytes, 0, bytes.length);
        byte[] finalBytes = new byte[savableDigest.getDigestSize()];
        savableDigest.doFinal(finalBytes, 0);
        String encoded = Base64.getUrlEncoder().encodeToString(finalBytes);
        return encoded.substring(0, 16);
    }

}
