package net.dingo.sv.controller;

import lombok.RequiredArgsConstructor;
import net.dingo.sv.connectors.CIDConnector;
import net.dingo.sv.connectors.SignConnector;
import net.dingo.sv.connectors.VerifyConnector;
import net.dingo.sv.domain.model.GenerateCIDResponse;
import net.dingo.sv.domain.model.GenerateKeysResponse;
import net.dingo.sv.domain.model.GetUserInfoResponse;
import net.dingo.sv.domain.model.SignResponse;
import net.dingo.sv.dto.request.GenerateCIDRequest;
import net.dingo.sv.dto.request.GenerateKeysRequest;
import net.dingo.sv.dto.request.GetUserInfoRequest;
import net.dingo.sv.dto.request.SignRequest;
import net.dingo.sv.dto.request.VerifyRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/gateway")
@RequiredArgsConstructor
public class GatewayController {
    private final SignConnector signConnector;
    private final VerifyConnector verifyConnector;
    private final CIDConnector cidConnector;

    @PostMapping("/sign")
    public ResponseEntity<String> sign(@RequestBody SignRequest request) {
        SignResponse signResponse = signConnector.signContent(request);
        return ResponseEntity.ok(signResponse.getSignature());
    }

    @PutMapping("/keys")
    public ResponseEntity<String> generateKeys(@RequestBody GenerateKeysRequest request) {
        GenerateKeysResponse generateKeysResponse = signConnector.generateKeys(request);
        return ResponseEntity.ok(generateKeysResponse.getBase64PubKey());
    }

    @PostMapping("/verify")
    public ResponseEntity<String> verify(@RequestBody VerifyRequest request) {
        boolean verify = verifyConnector.verify(request);
        return ResponseEntity.ok(String.valueOf(verify));
    }

    @PostMapping("/cid")
    public ResponseEntity<GenerateCIDResponse> generateCID(@RequestBody GenerateCIDRequest request) {
        GenerateCIDResponse generateCIDResponse = cidConnector.generateCID(request);
        return ResponseEntity.ok(generateCIDResponse);
    }

    @GetMapping("/cid/{nameID}")
    public ResponseEntity<GetUserInfoResponse> getUserInfo(@PathVariable("nameID") String nameID) {
        GetUserInfoResponse userInfo = cidConnector.getUserInfo(new GetUserInfoRequest(nameID));
        return ResponseEntity.ok(userInfo);
    }
}
