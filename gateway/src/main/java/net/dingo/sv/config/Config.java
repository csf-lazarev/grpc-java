package net.dingo.sv.config;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import net.dingo.sv.cid.CIDServiceGrpc;
import net.dingo.sv.sign.SignServiceGrpc;
import net.dingo.sv.verify.VerifyServiceGrpc;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    @Qualifier("signChannel")
    public ManagedChannel signChannel(@Value("${grpc.host}") String grpcHost,
                                      @Value("${grpc.sign.port}") int signChannelPort) {
        return ManagedChannelBuilder.forAddress(grpcHost, signChannelPort).usePlaintext().build();
    }

    @Bean
    public SignServiceGrpc.SignServiceBlockingStub signServiceBlockingStub(ManagedChannel signChannel) {
        return SignServiceGrpc.newBlockingStub(signChannel);
    }

    @Bean
    public VerifyServiceGrpc.VerifyServiceBlockingStub verifyServiceBlockingStub(@Value("${grpc.host}") String grpcHost,
                                                                                 @Value("${grpc.verify.port}") int verifyChannelPort) {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(grpcHost, verifyChannelPort).usePlaintext().build();
        return VerifyServiceGrpc.newBlockingStub(managedChannel);
    }

    @Bean
    public CIDServiceGrpc.CIDServiceBlockingStub cidServiceBlockingStub(@Value("${grpc.host}") String grpcHost,
                                                                        @Value("${grpc.cid.port}") int verifyChannelPort) {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress(grpcHost, verifyChannelPort).usePlaintext().build();
        return CIDServiceGrpc.newBlockingStub(managedChannel);
    }
}
