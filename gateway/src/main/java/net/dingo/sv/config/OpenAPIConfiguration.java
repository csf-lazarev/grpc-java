package net.dingo.sv.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "GRPC Gateway"
        ),
        servers = {
                @Server(url = "http://localhost:${server.port}")
        }
)
public class OpenAPIConfiguration {
}
