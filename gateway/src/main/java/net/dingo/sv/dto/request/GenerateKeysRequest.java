package net.dingo.sv.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenerateKeysRequest {
    private String cid;
}
