package net.dingo.sv.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenerateCIDRequest {
    private String name;
    private String nameID;
    private String description;
    private String cid;
}
