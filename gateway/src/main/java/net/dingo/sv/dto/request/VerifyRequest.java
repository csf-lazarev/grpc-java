package net.dingo.sv.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerifyRequest {
    private String cid;
    private String payload;
    private String base64Signature;
}
