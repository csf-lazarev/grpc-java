package net.dingo.sv.dto.request;

import lombok.Data;

@Data
public class SignRequest {
    private String cid;
    private String payload;
}
