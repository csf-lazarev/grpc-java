package net.dingo.sv.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler({Exception.class})
    public ResponseEntity<String> onException(Exception e, HttpServletRequest req) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
