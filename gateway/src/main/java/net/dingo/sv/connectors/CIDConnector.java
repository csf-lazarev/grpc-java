package net.dingo.sv.connectors;

import lombok.RequiredArgsConstructor;
import net.dingo.sv.cid.CIDServiceGrpc;
import net.dingo.sv.cid.Ciding;
import net.dingo.sv.domain.model.GenerateCIDResponse;
import net.dingo.sv.domain.model.GetUserInfoResponse;
import net.dingo.sv.dto.request.GenerateCIDRequest;
import net.dingo.sv.dto.request.GetUserInfoRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CIDConnector {
    private final CIDServiceGrpc.CIDServiceBlockingStub stub;

    public GenerateCIDResponse generateCID(GenerateCIDRequest request) {
        Ciding.CIDResponse cidResponse = stub.generateCID(Ciding.GenerateCID.newBuilder()
                .setCid(Optional.ofNullable(request.getCid()).orElse(""))
                .setDescription(request.getDescription())
                .setName(request.getName())
                .setNameID(request.getNameID())
                .build());
        String cid = cidResponse.getCid();
        String nameID = cidResponse.getNameID();
        return new GenerateCIDResponse(cid, nameID);
    }

    public GetUserInfoResponse getUserInfo(GetUserInfoRequest request) {
        Ciding.UserInfoResponse userInfo = stub.getUserInfo(Ciding.GetUserInfo.newBuilder()
                .setNameID(request.getNameID())
                .build());
        return new GetUserInfoResponse(userInfo.getNameID(), userInfo.getName(), userInfo.getDescription(), userInfo.getCid());
    }
}
