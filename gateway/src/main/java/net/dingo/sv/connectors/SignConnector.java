package net.dingo.sv.connectors;

import lombok.RequiredArgsConstructor;
import net.dingo.sv.domain.model.GenerateKeysResponse;
import net.dingo.sv.domain.model.SignResponse;
import net.dingo.sv.dto.request.GenerateKeysRequest;
import net.dingo.sv.dto.request.SignRequest;
import net.dingo.sv.sign.SignServiceGrpc;
import net.dingo.sv.sign.Signing;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignConnector {
    private final SignServiceGrpc.SignServiceBlockingStub stub;

    public SignResponse signContent(SignRequest request) {
        Signing.SignResponse grpcResponse = stub.sign(Signing.SignRequest.newBuilder()
                .setCid(request.getCid())
                .setPayload(request.getPayload()).build());
        return new SignResponse(grpcResponse.getBase64Signature());
    }

    public GenerateKeysResponse generateKeys(GenerateKeysRequest request) {
        Signing.GenKeyPairResponse genKeyPairResponse = stub.genKeyPair(Signing.GenKeyPairRequest.newBuilder()
                .setCid(request.getCid())
                .build());
        return new GenerateKeysResponse(genKeyPairResponse.getBase64PubKey());
    }
}
