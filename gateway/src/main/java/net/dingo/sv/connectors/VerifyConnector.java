package net.dingo.sv.connectors;

import lombok.RequiredArgsConstructor;
import net.dingo.sv.dto.request.VerifyRequest;
import net.dingo.sv.verify.Verification;
import net.dingo.sv.verify.VerifyServiceGrpc;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VerifyConnector {
    private final VerifyServiceGrpc.VerifyServiceBlockingStub stub;

    public boolean verify(VerifyRequest request) {
        Verification.VerifyResponse verify = stub.verify(Verification.VerifyRequest.newBuilder()
                .setCid(request.getCid())
                .setPayload(request.getPayload())
                .setSignature(request.getBase64Signature())
                .build());
        return verify.getVerified();
    }

}
