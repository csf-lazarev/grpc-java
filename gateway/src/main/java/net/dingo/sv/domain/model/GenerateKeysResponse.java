package net.dingo.sv.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GenerateKeysResponse {
    private String base64PubKey;
}
