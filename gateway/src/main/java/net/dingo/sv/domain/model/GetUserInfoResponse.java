package net.dingo.sv.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetUserInfoResponse {
    private String nameID;
    private String name;
    private String description;
    private String cid;
}
